<?php

require_once 'vendor/autoload.php';

use Symfony\Component\Translation\Translator;
$loader = new Symfony\Component\Translation\Loader\YamlFileLoader();
$resourceFiles = [
    'fr' => [
        __DIR__."/translations/messages.fr.yml",
    ],
];


// prime the cache
//$translator = $this->getTranslator($loader, ['cache_dir' => "/tmp/trans", 'resource_files' => $resourceFiles], 'yml');

$translator = new Translator('fr');
$translator->setFallbackLocales(['fr']);
$translator->addLoader('yml', $loader);
$translator->addResource('yml',  __DIR__."/translations/messages.fr.yml", 'fr');

echo $translator->trans('Hello World! %count%', ["%count%" => "4"]) . PHP_EOL; // outputs « Bonjour ! »