<?php

require_once 'vendor/autoload.php';

use Symfony\Component\Translation\Translator;
$loader = new Symfony\Component\Translation\Loader\ArrayLoader();
$translator = new Translator('fr_FR');
$translator->addLoader('array', $loader);
$translator->addResource('array', [
	"Hello World! %count% " => "Bonjour !  %count% ",
], 'fr_FR');

echo $translator->trans('Hello World! %count% ', ["%count%" => "4"]) . PHP_EOL; // outputs « Bonjour ! »